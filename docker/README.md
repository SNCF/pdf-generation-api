# Docker related documentation

## TODO
- Features
  - the API
  - the ressource server

## Chromium sandbox 

In order to keep Chromium sandbox active in our container :
  - we use seccomp to allow system calls used by Chromium (example : https://github.com/Zenika/alpine-chrome/blob/master/chrome.json)
  - we create and use an unprivileged user (docker) with his group (docker)

## Run and test the service

```sh
# Run the service
docker run -p 3000:3000 -p 3001:3001 --name pdf-generation --security-opt seccomp=.\docker\chrome-seccomp-profile.json registry.gitlab.com/sncf/pdf-generation-api:0.0.1
# Run in interactive mode
docker run -p 3000:3000 -p 3001:3001 --name pdf-generation --security-opt seccomp=.\docker\chrome-seccomp-profile.json -it registry.gitlab.com/sncf/pdf-generation-api:0.0.1 /bin/sh
# test the service
curl -POST -H "Content-Type: multipart/form-data" -F "template=@./example/simple.html" http://localhost:3000/pdf
# Load testing
seq 1 20 | xargs -n1 -P10 curl -POST -H "Content-Type: multipart/form-data" -F "template=@./example/simple.html" http://localhost:3000/pdf
```

## Known issues

### Out of memory in chrome

> :warning: currently under investigation

You can use the docker ipc setting (https://docs.docker.com/engine/reference/run/#ipc-settings---ipc), example :
```sh
docker run -p 3000:3000 -p 3001:3001 --ipc=host --security-opt seccomp=.\docker\chrome-seccomp-profile.json pdf-generation-api:0.0.0
```
