#!/bin/sh
cd /services/pdf-generation-api && node dist/main &
cd /ressources && http-server --port 3001 -a 0.0.0.0 --cors &
# Wait for any process to exit
wait -n
# Exit with status of process that exited first
exit $?

