export class GeneratePdfDto
{
    private _templateHtml: string;

    constructor(fichierAConvertir: string)
    {
        this._templateHtml = fichierAConvertir;
    }


    get templateHtml(): string
    {
        return this._templateHtml;
    }
}
