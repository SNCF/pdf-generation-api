import * as fs from "fs";
import { Test, TestingModule } from '@nestjs/testing';
import { PdfPuppeteerService } from "./pdf-puppeteer.service";
import { Readable } from "stream";
import { PDFExtract, PDFExtractResult } from 'pdf.js-extract';
import { ConfigurationService } from '../../../../config/configuration.service';

describe('TestPdfPuppeteerService', () => {
    let pdfPuppeteerService: PdfPuppeteerService;
    let configurationService: ConfigurationService;
    let module: TestingModule;

    beforeAll(async () => {
        module = await Test.createTestingModule({
            providers: [
                PdfPuppeteerService,
                {
                    provide: ConfigurationService,
                    useValue: {
                        getConfiguration: jest.fn(() => {
                            //mocked endpoints
                            return {authorizedEndpoints: "http://localhost:3001, https://unpkg.com/pagedjs"};
                        })
                    }
                }
            ],
        }).compile();
        pdfPuppeteerService = await module.resolve(PdfPuppeteerService);
    }, 30000);

    afterAll(async () => {
        await pdfPuppeteerService.closeBrowser();
    });

    describe('get authorize endpoints from config', () => {
        it('should return null when there is no config', async () => {
            configurationService = await module.get<ConfigurationService>(ConfigurationService);
            jest.spyOn(configurationService, 'getConfiguration').mockImplementation(() => {
                return {};
            });

            const endpointsRetrieved = pdfPuppeteerService.getAuthorizedEndpointsFromConfig();
            expect(endpointsRetrieved).toBe(null);
        });

        it('should return array with one endpoint when there is one endpoint in config', async () => {
            configurationService = await module.get<ConfigurationService>(ConfigurationService);
            jest.spyOn(configurationService, 'getConfiguration').mockImplementation(() => {
                return {authorizedEndpoints: "http://localhost:3001"};
            });

            const endpointsRetrieved = pdfPuppeteerService.getAuthorizedEndpointsFromConfig();
            expect(endpointsRetrieved).toStrictEqual(["http://localhost:3001"]);
        });

        it('should return same arrays when there is two endpoints comma separated in config', async () => {
            //when
            configurationService = await module.get<ConfigurationService>(ConfigurationService);
            jest.spyOn(configurationService, 'getConfiguration').mockImplementation(() => {
                return {authorizedEndpoints: "http://localhost:3001,https://unpkg.com/pagedjs"};
            });
            const endpointsExpected = ["http://localhost:3001", "https://unpkg.com/pagedjs"];
            const endpointsRetrieved: string[] = pdfPuppeteerService.getAuthorizedEndpointsFromConfig();

            //then
            expect(endpointsRetrieved).toStrictEqual(endpointsExpected);
        });

        it('should return same arrays when there is two endpoints comma and space separated in config', async () => {
            configurationService = await module.get<ConfigurationService>(ConfigurationService);
            jest.spyOn(configurationService, 'getConfiguration').mockImplementation(() => {
                return {authorizedEndpoints: "http://localhost:3001 ,   https://unpkg.com/pagedjs"};
            });

            const endpointsRetrieved = pdfPuppeteerService.getAuthorizedEndpointsFromConfig();
            expect(endpointsRetrieved).toStrictEqual(["http://localhost:3001", "https://unpkg.com/pagedjs"]);
        });

        it('should not remove spaces in url', async () => {
            configurationService = await module.get<ConfigurationService>(ConfigurationService);
            jest.spyOn(configurationService, 'getConfiguration').mockImplementation(() => {
                return {authorizedEndpoints: "http://localhost:3001 ,   https://unpkg.com/paged js"};
            });

            const endpointsRetrieved = pdfPuppeteerService.getAuthorizedEndpointsFromConfig();
            expect(endpointsRetrieved).toStrictEqual(["http://localhost:3001", "https://unpkg.com/paged js"]);
        });
    });

    describe('isAllowedEndpoint', () => {
        it('should return true when an endpoint have the same begin characters that another in authorized endpoints', async () => {
            //when
            const endpoint = 'http://localhost:3001/resource';
            const enable: boolean = pdfPuppeteerService.isAllowedEndpoint(endpoint);

            //then
            expect(enable).toBe(true);
        });

        it('should return true when an endpoint is the same that another in authorized endpoints', async () => {
            //when
            const endpoint = 'https://unpkg.com/pagedjs@0.2.0/dist/paged.js';
            const enable: boolean = pdfPuppeteerService.isAllowedEndpoint(endpoint);

            //then
            expect(enable).toBe(true);
        });

        it("should return false when an endpoint haven't the same begin characters that any in authorized endpoints", async () => {
            //when
            const endpoint = 'https://google.com';
            const enable: boolean = pdfPuppeteerService.isAllowedEndpoint(endpoint);
            //then
            expect(enable).toBe(false);
        });
    });

    describe("isAllowedEndpoint when there is no AUTHORIZE_ENDPOINTS configkey", () => {
        let pdfPuppeteerServiceWithoutAuthorizedEndpoints: PdfPuppeteerService;
        beforeAll(async () => {
            const moduleWithoutAuthorizedEndpoints = await Test.createTestingModule({
                providers: [
                    PdfPuppeteerService,
                    {
                        provide: ConfigurationService,
                        useValue: {
                            getConfiguration: jest.fn(() => {
                                //mocked endpoints
                                return {authorizedEndpoints: null};
                            })
                        }
                    }
                ],
            }).compile();
            pdfPuppeteerServiceWithoutAuthorizedEndpoints = await moduleWithoutAuthorizedEndpoints.resolve(PdfPuppeteerService);
        }, 30000);

        it('should return false when no authorized endpoints in config', function () {
            //when
            const endpoint = 'https://google.com';
            const enable: boolean = pdfPuppeteerServiceWithoutAuthorizedEndpoints.isAllowedEndpoint(endpoint);
            //then
            expect(enable).toBe(false);
        });
    });

    describe('generatePdf', () => {
        jest.setTimeout(20000);
        fs.mkdir('./test/pdfGenerated', _ => {});

        it('should return pdf with "Hello World" content inside in', async () => {
            const result: Readable = await pdfPuppeteerService.generate('<h1>Hello World</h1>');
            await new Promise((res) => result.pipe(fs.createWriteStream('./test/pdfGenerated/helloworld.pdf').on('close', res)));
            expect(result).toBeDefined();
            const pdfExtract = new PDFExtract();
            await pdfExtract.extractBuffer(fs.readFileSync('./test/pdfGenerated/helloworld.pdf')).then((page: PDFExtractResult) => {
                expect(page.pages.length).toBe(1);
                expect(page.pages.at(0).content.at(0).str).toBe('Hello World');
            });
        });

        it('should return pdf of two pages', async () => {
            const result: Readable = await pdfPuppeteerService.generate(fs.readFileSync('./test/templateHtml/todolist.html').toString());
            await new Promise((res) => result.pipe(fs.createWriteStream('./test/pdfGenerated/todolist.pdf').on('close', res)));
            expect(result).toBeDefined();
            const pdfExtract = new PDFExtract();
            await pdfExtract.extractBuffer(fs.readFileSync('./test/pdfGenerated/todolist.pdf')).then((page: PDFExtractResult) => {
                expect(page.pages.length).toBe(2);
            });
        });

        it('should return pdf with number of pages in bottom of footer', async () => {
            const result: Readable = await pdfPuppeteerService.generate(fs.readFileSync('./test/templateHtml/docWithNumbering.html', 'utf-8'));
            await new Promise((res) => result.pipe(fs.createWriteStream('./test/pdfGenerated/docWithNumbering.pdf').on('close', res)));
            expect(result).toBeDefined();
            const pdfExtract = new PDFExtract();
            await pdfExtract.extractBuffer(fs.readFileSync('./test/pdfGenerated/docWithNumbering.pdf')).then((page: PDFExtractResult) => {
                //content have number of page
                expect(page.pages.at(0).content.at(0).str).toContain('Page 1 sur 1');
            });
        });

        it('should return pdf with running element in bottom center of pdf', async () => {
            const result: Readable =
                await pdfPuppeteerService.generate(fs.readFileSync('./test/templateHtml/runningElements.html', 'utf-8'));
            await new Promise((res) => result.pipe(fs.createWriteStream('./test/pdfGenerated/runningElements.pdf').on('close', res)));
            expect(result).toBeDefined();
            const pdfExtract = new PDFExtract();
            await pdfExtract.extractBuffer(fs.readFileSync('./test/pdfGenerated/runningElements.pdf')).then((page: PDFExtractResult) => {
                expect(page.pages.at(0).content.at(0).str).toContain('e.SNCF / Direction Design & Développement');
                //content situated in the footer of the page
                expect(page.pages.at(0).content.at(0).y > page.pages.at(0).pageInfo.height - 50).toBeTruthy();
            });
        });
    });
});
