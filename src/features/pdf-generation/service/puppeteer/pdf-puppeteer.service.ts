import * as fs from "fs";
import * as puppeteer from "puppeteer";
import { Browser, ConsoleMessage, Page } from "puppeteer";
import { Injectable, Logger, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { Readable } from "stream";
import { PdfIOUtils } from "../../utils/pdf-i-o-utils";
import { ConfigurationService } from '../../../../config/configuration.service';

@Injectable()
export class PdfPuppeteerService implements OnModuleInit, OnModuleDestroy {
    private readonly logger = new Logger(PdfPuppeteerService.name);

    private browser: Browser;

    private authorizedEndpoints: string[] | null;

    constructor(private readonly configurationService: ConfigurationService){
        this.authorizedEndpoints = this.getAuthorizedEndpointsFromConfig();
    }

    getAuthorizedEndpointsFromConfig() {
        // ?. operator return undefined when get("AUTHORIZES_ENDPOINTS") return null . We need to force to null return with => ?? null
        return this.configurationService
            .getConfiguration()
            .authorizedEndpoints
            ?.split(",")
            ?.map((url) => url.trim()) ?? null;
    }

    async onModuleInit(): Promise<void> {
        await this.initBrowser();
    }

    async onModuleDestroy(): Promise<void> {
        await this.closeBrowser;
    }

    async closeBrowser(): Promise<void> {
        if (this.browser) {
            await this.browser.close();
        }
    }

    async initBrowser() {
        if(!this.browser) this.browser = await puppeteer.launch({headless: true})
    }

    public async generate(templateHtml: string): Promise<Readable> {
        try{
            await this.initBrowser();
            const page = await this.browser.newPage();
            this.logger.log(`Begin of the pdf generation`);
            this.logChromeConsoleErrors(page);
            await this.requestInterceptionHandler(page);
            await page.setContent(templateHtml, {waitUntil: "networkidle0"});
            await this.addPagedjsScriptsToPage(page);
            await this.onPagedJsRenderComplete(page);
            const pdfBuffer = await page.pdf({printBackground: true});
            await page.close();
            this.logger.log('End of the pdf generation');
            return PdfIOUtils.readableStream(pdfBuffer);
        }catch (e) {
            this.logger.error("Error during the generation of pdf", e);
            throw new Error("Error during the generation of pdf");
        }
    }

    private logChromeConsoleErrors(page: Page) {
        page.on("console", (reason:  ConsoleMessage) => {
            this.logger.log(`Chrome Headless console: type=${reason.type()} text=${reason.text()} stackTrace_url=${reason.stackTrace().at(0).url}`);
        });
    }

    isAllowedEndpoint(endpoint: string): boolean{
        return this.authorizedEndpoints?.some(authorizeEndpoint => endpoint.startsWith(authorizeEndpoint)) ?? false;
    }

    /**
     * Allow to filter request that the template html could make (when it search fonts, images etc)
     * @param page to setRequestInterception in
     * @private
     */
    private async requestInterceptionHandler(page: Page) {
        await page.setRequestInterception(true);
        page.on('request', request => {
            if (this.isAllowedEndpoint(request.url()) ) {
                page.on('response', response => {
                    this.logger.debug(`Endpoint authorized: ${request.method()} ${response.status()} ${response.url()}`);
                });
                request.continue();
            } else {
                page.on('response', response => {
                    this.logger.log(`Endpoint not authorized: ${request.method()} ${response.status()} ${response.url()}`);
                });
                request.abort("connectionrefused");
            }
        });
    }

    /**
     * Add pagedjs scripts to page
     * @param page where the scripts is includes
     * @private
     */
    private async addPagedjsScriptsToPage(page: Page) {
        const scriptPathsToAdd = ["./node_modules/pagedjs/dist/paged.polyfill.js"];
        const scripts = scriptPathsToAdd.map((scriptPath) => fs.readFileSync(scriptPath, "utf-8"));
        await page.evaluate(  async (scripts: []) => {
            scripts.forEach(script =>{
                const scriptElement = document.createElement('script');
                scriptElement.text = script;
                document.body.appendChild(scriptElement);
            });
        }, scripts);
    }

    /**
     * Wait until pagedjs polyfill script finish pagination of the document
     * @param page where the script is includes
     * @param timeout of how many puppeteer can wait pagedjs polyfill rendering
     * @private
     */
    private async onPagedJsRenderComplete(page: Page, timeout: number = 30) {
        //TODO: externalize timeout param in config
        let renderedPagedJsEvent = "rendered";
        return Promise.race([
            page.evaluate((eventName) => {
                return new Promise<void>(function(resolve) {
                    //@ts-ignore
                    window.PagedPolyfill.on(eventName, () => {
                        resolve(); // resolves when the event fires
                    });
                });
            }, renderedPagedJsEvent),
            page.waitForTimeout(timeout * 1000)
        ]);
    }
}
