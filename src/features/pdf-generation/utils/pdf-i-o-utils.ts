import {Readable} from "stream";
import fs from "fs";

/**
 * Utilities for reading and writing PDF files into buffers and streams
 */
export class PdfIOUtils {
    /**
     * Create readable stream from buffer
     * @param pdfBuffer buffer of generated pdf
     */
    public static readableStream(pdfBuffer: Buffer): Readable {
        const stream = new Readable();
        stream.push(pdfBuffer);
        stream.push(null); //indicate that the stream must be completed
        return stream;
    }

    /**
     * Create buffer with the pdf file passed by paramater
     * @param pdfFile that we want to store in buffer
     * @private
     */
    public static async createBuffer(pdfFile: string): Promise<Buffer> {
        return fs.readFileSync(pdfFile);
    }
}