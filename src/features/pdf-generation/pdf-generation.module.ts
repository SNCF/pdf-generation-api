import { Module } from '@nestjs/common';
import { PdfController } from './controller/pdf.controller';
import { PdfPuppeteerService } from './service/puppeteer/pdf-puppeteer.service';
import { ConfigurationService } from '../../config/configuration.service';


@Module({
    providers: [
        PdfPuppeteerService,
        ConfigurationService,
    ],
    controllers: [
        PdfController
    ]
})
export class PdfGenerationModule {
}
