import { Module } from '@nestjs/common';
import { PdfGenerationModule } from './features/pdf-generation/pdf-generation.module';
import { ConfigModule } from '@nestjs/config';
import { ConfigurationService } from './config/configuration.service';

@Module({
    imports: [
        PdfGenerationModule,
        ConfigModule.forRoot({
            isGlobal: true,
            load: [ConfigurationService.load()],
        })
    ],
})
export class AppModule {
}
