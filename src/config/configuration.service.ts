import * as yaml from 'js-yaml';
import { Injectable } from '@nestjs/common';
import { ConfigService, registerAs } from '@nestjs/config';
import { readFileSync } from 'fs';
import { join } from 'path';
import { Configuration, LogConfiguration } from './configuration';

@Injectable()
export class ConfigurationService {

    private static readonly NAMESPACE_GLOBAL = 'conf';

    public static readonly DEFAULT_PATH: string = 'config';
    public static readonly DEFAULT_FILE_NAME: string = 'config.yml';

    public static load() {
        return registerAs(
            ConfigurationService.NAMESPACE_GLOBAL,
            (): Record<string, any> => {
                try {
                    return yaml.load(
                        readFileSync(join(ConfigurationService.DEFAULT_PATH, ConfigurationService.DEFAULT_FILE_NAME), 'utf8')
                    ) as Record<string, any>;
                } catch (err) {
                    if (err.code === 'ENOENT') {
                        console.log('No configuration file');
                    } else {
                        throw err;
                    }
                }
            }
        );
    }

    constructor(private configService: ConfigService) {
    }

    public getConfiguration(): Configuration {
        return this.configService.get<Configuration>(ConfigurationService.NAMESPACE_GLOBAL, {infer: true}) || {};
    }

    public getLogConfiguration(): LogConfiguration {
        return this.configService.get<LogConfiguration>(ConfigurationService.NAMESPACE_GLOBAL + '.log', {infer: true}) || {};
    }
}