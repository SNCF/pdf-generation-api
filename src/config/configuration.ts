/**
 * Logging configuration to log in a file
 */
export interface LogFileConfiguration {
    /**
     * Date pattern used for files name
     *
     * default: 'YYYYMMDD'
     */
    datePatternForFileName?: string;
    /**
     * Max file size for rotation
     *
     * default: '10m'
     */
    maxSize?: string;
    /**
     * Max file for rotation
     *
     * default: '60d'
     */
    maxFiles?: string;
    /**
     * Log file name
     *
     * default: 'logs/pdf-generation-api-%DATE%.log'
     */
    name?: string;
}

/**
 * Logging configuration
 */
export interface LogConfiguration {
    /**
     * Configuration to log in a file. If true we use default configuration.
     *
     * default: true
     */
    file?: LogFileConfiguration | boolean;
    /**
     * Winston log level
     *
     * default: info
     */
    level?: string;
}

export interface Configuration {
    /**
     * List of endpoint authorized in the HTML template separated by a comma, example : "https://google.com,https://pagedjs.org"
     *
     * default: empty, the template can't use external resources
     */
    authorizedEndpoints?: string;
    log?: LogConfiguration;
}