# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

for new features.

### Changed
Upgrade Dependencies
- **puppeteer**: update from v13.6.0 to v19.5.2
- **nestjs**: update from v8.*.* to v9.*.*
- **type/puppeteer**: remove it, because it is a stub types definition and Puppeteer provides its own type definitions
- upgrage other dependencies of `package.json`

### Deprecated

for soon-to-be removed features.

### Removed

for now removed features.

### Fixed
- fix looping issue when a html template which have table with large content into cell 
  - => **pagedjs**: update from v0.3.2 to v4.1.0 to embed the fix of looping issue https://gitlab.coko.foundation/pagedjs/pagedjs/-/merge_requests/188
- fix style issue cause by the repeated header feature

### Security

in case of vulnerabilities
